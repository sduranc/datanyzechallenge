package test.pages;

import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.By;

import java.util.List;
import java.util.Map;

/**
 * Created by sebastianduran on 17-01-17.
 */
@DefaultUrl("http://www.datanyze.com/settings")
public class SettingsPage extends DatanyzePage {

    public SettingsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath="/html/body/div[5]/div[1]/ul/li[3]/a")
    WebElement alertsTab;

    @FindBy(xpath="/html/body/div[5]/div[2]/form[1]/div[1]/span/span[1]/span/ul/li/input")
    WebElement webTrackingSearchFilter;

    @FindBy(id="ob-alert")
    WebElement addTechSubmit;

    @FindBy(xpath="/html/body/span/span/span[1]/input")
    WebElement alertFilterInput;

    @FindBy(id="targeting_button")
    WebElement saveAlertFilters;

    public void goToAlerts() {
        element(alertsTab).click();
    }

    public void addTechnologiesToTrack(List<String> techList) {

        for (int i = 0; i < techList.size(); i++) {
            element(webTrackingSearchFilter).type(techList.get(i));
            element(getDriver().findElement(By.xpath("//span[contains(text(), '"+techList.get(i)+"')]"))).click();
        }
        addTechSubmit.submit();

    }

    public void addAlertFilters(Map<String, String> filters) {
        int i = 2;
        for (Map.Entry<String, String> entry : filters.entrySet()){
            element(getDriver().findElement(By.xpath("//span[.='Select a Condition']"))).click();
            element(alertFilterInput).type(entry.getKey());
            element(getDriver().findElement(By.xpath("//li[.='"+entry.getKey()+"']"))).click();
            element(getDriver().findElement(By.xpath("//*[@id='query-container']/div["+i+"]/div[4]/span[2]/span[1]/span/ul/li/input"))).type(entry.getValue());
            element(getDriver().findElement(By.xpath("//li[.='"+entry.getValue()+"']"))).click();
            i++;
        }
        element(saveAlertFilters).click();
    }
}
