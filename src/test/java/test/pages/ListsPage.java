package test.pages;

import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.By;

/**
 * Created by sebastianduran on 17-01-17.
 */
@DefaultUrl("http://www.datanyze.com/lists")
public class ListsPage extends DatanyzePage{
    public ListsPage(WebDriver driver) {
        super(driver);
    }

    public boolean checkIfListExistsByName(String name){
        return isElementVisible(By.xpath("//a[contains(text(), '"+name+"')]"));
    }
}
