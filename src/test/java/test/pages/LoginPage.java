package test.pages;

import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
/**
 * Created by sebastianduran on 16-01-17.
 */
@DefaultUrl("http://www.datanyze.com/login")
public class LoginPage extends DatanyzePage {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(name="email")
    WebElement emailInput;

    @FindBy(name="password")
    WebElement passInput;

    @FindBy(xpath="//*[@id='user_login']/div[3]/input")
    WebElement loginSubmit;

    public void login(String email, String pass){
        emailInput.clear();
        element(emailInput).type(email);
        passInput.clear();
        element(passInput).type(pass);

        loginSubmit.submit();
    }
}