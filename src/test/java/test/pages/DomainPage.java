package test.pages;


import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.By;
/**
 * Created by sebastianduran on 17-01-17.
 */
@DefaultUrl("http://www.datanyze.com/domain")
public class DomainPage extends DatanyzePage {
    public DomainPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath="//a[contains(text(), 'Company')]")
    WebElement companyTab;

    @FindBy(xpath="//a[contains(text(), 'People')]")
    WebElement peopleTab;

    @FindBy(id="check_all_none")
    WebElement selectAllEmployees;

    @FindBy(id="select2-add_to_list-container")
    WebElement addToListSelect;

    @FindBy(xpath="//li[contains(text(), 'Create another list')]")
    WebElement createNewList;

    @FindBy(xpath="//div[@id='create-list-modal']//child::input[@id='list_name']")
    WebElement listNameInput;

    @FindBy(xpath="//div[@id='create-list-modal']//child::input[@value='Create list']")
    WebElement createListSubmit;

    public void viewCompanyTab() {
        companyTab.click();    
    }

    public void viewPeopleTab() {
        peopleTab.click();
    }

    public void addEmployeesToNewList(String listName) {
        selectAllEmployees.click();
        addToListSelect.click();
        createNewList.click();
        element(listNameInput).type(listName);
        createListSubmit.click();
    }
}
