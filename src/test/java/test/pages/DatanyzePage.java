package test.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

/**
 * Created by sebastianduran on 16-01-17.
 */
public abstract class DatanyzePage extends PageObject{
    public DatanyzePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath="//*[@id='main-search-box']/form/span/input")
    WebElement searchBox;

    @FindBy(xpath="//*[@class='logout']")
    WebElement logout;

    public void searchDomainOrTech(String name){
        element(searchBox).type(name);
        element(searchBox).sendKeys(Keys.RETURN);
    }

    public void logout(String user){
        element(getDriver().findElement(By.xpath("//span[contains(text(), '"+user+"')]"))).click();
        logout.click();
    }
}
