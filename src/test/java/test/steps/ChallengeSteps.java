package test.steps;

import test.pages.DomainPage;
import test.pages.ListsPage;
import test.pages.SettingsPage;
import test.pages.LoginPage;

import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;
/**
 * Created by sebastianduran on 16-01-17.
 */
public class ChallengeSteps extends ScenarioSteps {

    @Managed(driver = "chrome")
    WebDriver driver;

    LoginPage loginPage;
    SettingsPage settingsPage;
    DomainPage domainPage;
    ListsPage listsPage;

    public final static String user = "duran.c.sebastian@gmail.com";
    public final static String pass = "d4t4nyz3";

    @Step
    @Given("the user logs into datanyze")
    public void log_into_datanyze(){
        loginPage.open();
        loginPage.login(user, pass);
    }

    @Step
    @When("he adds two new technologies to track")
    public void add_technologies_to_track(){
        List<String> techList = new ArrayList<String>();
        techList.add("Google translate API");
        techList.add("Google Play");
        settingsPage.open();
        settingsPage.goToAlerts();
        settingsPage.addTechnologiesToTrack(techList);
    }

    @Step
    @When("adds two new alert filters")
    public void add_new_alert_filters(){
        Map filters = new HashMap();
        filters.put("Country", "United States");
        filters.put("Industry", "Internet");
        settingsPage.addAlertFilters(filters);
    }

    @Step
    @When("searches for domain")
    public void search_domain(){
        domainPage.searchDomainOrTech("datanyze.com");
    }

    @Step
    @Then("he should be able to add the employees to a new list")
    public void adding_all_employees_to_new_list(){
        domainPage.viewCompanyTab();
        domainPage.viewPeopleTab();
        domainPage.addEmployeesToNewList("Test Company Prospects");
        listsPage.open();
        assertTrue(listsPage.checkIfListExistsByName("Test Company Prospects"));
        domainPage.logout(user);
    }
}
